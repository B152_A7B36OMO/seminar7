package seminar7;

/**
 *
 * @author drchaj1
 */
public class Employee {
    public static final double PT_EMPLOYEE_HOUR_SALARY = 150.0;
    public static final double STANDARD_EMPLOYEE_SALARY = 20000.0;

    final public static int STANDARD_EMPLOYEE_TYPE = 1;
    final public static int PT_EMPLOYEE_TYPE = 2;

    final private String name;
    final private int type;
    final private double hours;

    public Employee(String name, int type, double hours) {
        assert type == STANDARD_EMPLOYEE_TYPE ||
                type == PT_EMPLOYEE_TYPE;
        this.name = name;
        this.type = type;
        this.hours = hours;
    }
    
    public Employee(String name, int type) {
        this(name, type, 0.0);
    }
    
    public double salary() {
        //DOPLNTE
        return 0.0;
    }
    
    public String payout() {
        //DOPLNTE
        return "";
    }

    @Override
    public String toString() {
        String s = name;
        switch(type) {
            case STANDARD_EMPLOYEE_TYPE:
                s += "STANDARD: ";
                break;
            case PT_EMPLOYEE_TYPE:
                s += "PART TIME (" + hours + "): ";
                break;
            default:
                throw new IllegalStateException("Unknown employee type: " + type);
        }
        s += " " + salary();
        return s;
    }
       
}
