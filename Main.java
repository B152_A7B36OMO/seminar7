package seminar7;

/**
 *
 * @author drchaj1
 */
public class Main {
    public static void main(String[] args) {
        Employee[] emps = new Employee[]{
            new Employee("Adam", Employee.STANDARD_EMPLOYEE_TYPE),
            new Employee("Jirka", Employee.PT_EMPLOYEE_TYPE, 80.0),
            new Employee("Zdenek", Employee.PT_EMPLOYEE_TYPE, 240.0)
        };
        for(Employee e: emps) {
            System.out.println(e);
        }
    }
}